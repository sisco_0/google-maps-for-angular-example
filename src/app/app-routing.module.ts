import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddressAutocompleteComponent } from './address-autocomplete/address-autocomplete.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: '', component: HomeComponent },
  { path: 'address-autocomplete', component: AddressAutocompleteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
